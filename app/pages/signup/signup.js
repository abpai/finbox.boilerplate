/**
 * CSS Dependencies
 */
import 'base/index.css'
import 'jade-mixins/footer/index.css'
import './signup.css'

/**
 * JS Dependencies
 */

import 'base'

const www_url = 'https://finbox.io'

const signup_form = document.querySelector('.signup-form')
signup_form.addEventListener('submit', handle_submit)

export default function handle_submit (e) {
  if (e && e.preventDefault) e.preventDefault()
  const email = document.querySelector('.signup-form--email').value
  window.location = `${www_url}/signup?email=${encodeURIComponent(email)}`
}
