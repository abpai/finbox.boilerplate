/**
 * Initialize `protect`
 * @return {Generator} next
 */

function protect () {
  return function * protect (next) {
    var user = this.state.user

    if (user) {
      return yield next
    } else {
      // protected area, get the fuck out!
      this.redirect('/login?redirect=' + encodeURIComponent(this.originalUrl))
    }
  }
}

/**
 * Export `protect`
 */

module.exports = protect
