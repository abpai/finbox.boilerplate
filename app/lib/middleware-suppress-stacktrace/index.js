module.exports = function (statuses) {
  return function * (next) {
    try {
      yield next
    } catch (err) {
      var index = statuses.indexOf(err.status)
      if (index >= 0) {
        this.status = err.status
        this.body = err.message || '' + err
      } else {
        throw err
      }
    }
  }
}
