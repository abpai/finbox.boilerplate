/* global describe, it */

/**
 * Module Dependencies
 */

import { expect } from 'chai'
import to_query from './../to-query'

/**
 * Tests
 */

describe('to query', () => {
  it('should parse ?hello=true to { hello: true }', (next) => {
    expect(to_query('?hello=true')).to.deep.equal({ hello: 'true' })
    next()
  })

  it('should parse ?hello=true&below=true to { hello: true, below: true }', (next) => {
    expect(to_query('?hello=true&below=true')).to.deep.equal({ hello: 'true', below: 'true' })
    next()
  })

  it('should parse return empty object for empty string', (next) => {
    expect(to_query('')).to.deep.equal({})
    next()
  })
})
