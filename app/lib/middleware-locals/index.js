/**
 * Export `locals`
 */

module.exports = locals

/**
 * Make user locals is present as an object
 */

function * locals (next) {
  this.locals = this.locals || {}
  yield next
}
