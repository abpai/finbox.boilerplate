/**
 * Module Dependencies
 */

import localenv from 'localenv/noload'
import { join } from 'path'

/**
 * Environment
 */

import is_production from 'is-production'
const config = process.env.CONFIG || 'local'

/**
 * Paths
 */

const root = join(__dirname, '..', '..', '..')
const env = join(root, '.env')
const env_local = join(root, `.env.${config}`)

/**
 * Inject environment variables
 */

if (is_production()) {
  localenv.inject_env(env_local)
} else {
  localenv.inject_env(env_local)
  localenv.inject_env(env)
}

/**
 * Resolve references like ENV_VAR_A=$ENV_VAR_B
 */
for (var envvar in process.env) {
  process.env[envvar] = process.env[envvar]
    .replace(/\$(\{)?([A-Za-z0-9_]+)(\})?/g, (match, g1, g2, g3) => process.env[g2])
}
