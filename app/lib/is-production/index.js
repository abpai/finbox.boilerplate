/**
 * Are we running in production?
 */

module.exports = function () {
  var node_env = process.env.NODE_ENV || ''
  return node_env === 'production'
}
