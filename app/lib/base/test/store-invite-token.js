/* global describe, it */

/**
 * Module Dependencies
 */

import { should } from 'chai'
import store_invite_token from './../lib/store-invite-token'
import nock from 'nock'
import storage from 'storage'

/**
 * Tests
 */

should()
describe('store-invite-token', () => {
  it('should store iuid and post click', (next) => {
    const api_url = 'https://api.io'
    const iuid = 'some-unique-id'
    const query = { iuid }

    nock(api_url)
      .post(`/invites/${iuid}/click`)
      .reply(200)

    store_invite_token({ query, api_url })
      .then(() => {
        storage.getItem('iuid').should.equal(iuid)
        next()
      })
  })

  it('should do nothing', () => {
    const iuid = null
    const query = { iuid }

    store_invite_token({ query })
  })
})
