/* global describe, it, beforeEach */

/**
 * Module Dependencies
 */

import 'localenv'
import cookies from 'cookies-js'
import { should } from 'chai'
import track_click from './../lib/track-click'
import nock from 'nock'
import storage from 'storage'

/**
 * Tests
 */

should()
describe('track-click', () => {
  beforeEach(() => {
    storage.clear()
    cookies.set('partners.pi', '')
    cookies.set('partners.ai', '')
    cookies.set('partners.vi', '')
  })

  it('should register click with partners api', (next) => {
    const api_url = 'https://partners.io'
    const pi = 'program-id'
    const ai = 'affiliate-id'
    const query = { pi, ai }

    nock(api_url)
      .post('/clicks', (payload) => {
        payload.program_id.should.equal(pi)
        payload.affiliate_id.should.equal(ai)
        return true
      })
      .reply(200, { ok: true })

    track_click({ api_url, query })
      .then((body) => {
        const pid = storage.getItem('partners.pi')
        const aid = storage.getItem('partners.ai')
        const vid = storage.getItem('partners.vi')
        const len = (vid.length >= 7)

        cookies.get('partners.pi').should.equal(pid)
        cookies.get('partners.ai').should.equal(aid)
        cookies.get('partners.vi').should.equal(vid)

        pid.should.equal(pi)
        aid.should.equal(ai)
        len.should.be.true

        body.ok ? next() : next(new Error('failed'))
      })
      .catch(next)
  })

  it('should register click with user._id if user present', (next) => {
    const pi = 'program-id'
    const ai = 'affiliate-id'
    const api_url = 'https://partners.io'
    const query = { pi, ai }
    const user = { _id: 1 }

    nock(api_url)
      .post('/clicks', (payload) => {
        payload.program_id.should.equal(pi)
        payload.affiliate_id.should.equal(ai)
        payload.visitor_id.should.equal(String(user._id))
        return true
      })
      .reply(200, { ok: true })

    track_click({ api_url, query, user })
      .then((body) => {
        const pid = storage.getItem('partners.pi')
        const aid = storage.getItem('partners.ai')
        const vid = storage.getItem('partners.vi')

        cookies.get('partners.pi').should.equal(pid)
        cookies.get('partners.ai').should.equal(aid)
        cookies.get('partners.vi').should.equal(`${vid}`)

        pid.should.equal(pi)
        aid.should.equal(ai)
        vid.should.equal(String(1))

        body.ok ? next() : next(new Error('failed'))
      })
      .catch(next)
  })

  it('should clean tokens before sending payload to api', (next) => {
    const api_url = 'https://partners.io'
    const pi = '&program-id/'
    const ai = '~affiliate-id/'
    const user = { _id: '1/' }
    const query = { pi, ai }

    const _pi = 'program-id'
    const _ai = 'affiliate-id'
    const _user_id = '1'

    nock(api_url)
      .post('/clicks', (payload) => {
        payload.program_id.should.equal(_pi)
        payload.affiliate_id.should.equal(_ai)
        payload.visitor_id.should.equal(_user_id)
        return true
      })
      .reply(200, { ok: true })

    track_click({ api_url, query, user })
      .then((body) => {
        const pid = storage.getItem('partners.pi')
        const aid = storage.getItem('partners.ai')
        const vid = storage.getItem('partners.vi')

        cookies.get('partners.pi').should.equal(pid)
        cookies.get('partners.ai').should.equal(aid)
        cookies.get('partners.vi').should.equal(`${vid}`)

        pid.should.equal(_pi)
        aid.should.equal(_ai)
        vid.should.equal(_user_id)

        body.ok ? next() : next(new Error('failed'))
      })
      .catch(next)
  })

  it('should do nothing', () => {
    track_click({}).should.be.false
  })
})
