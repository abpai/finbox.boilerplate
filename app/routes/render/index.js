/**
 * Module Dependencies
 */

import Yoo from 'yoo'

/**
 * Yoo
 */

const yoo = Yoo(__dirname)
export default yoo

/**
 * Routes
 */

yoo.get('/:page', render)

/**
 * Route processing generator functions
 */

function * render (next) {
  const { page } = this.params
  this.body = yield yoo.render(`./../../pages/${page}/${page}.jade`)
}
