FROM mhart/alpine-node

ENV PATH=$PATH:/usr/src/node_modules/.bin
ARG NODE_ENV=production
ARG BABEL_ENV=production

# Set up .npmrc
ARG LR_NPM_TOKEN
RUN echo "registry=https://registry.npmjs.org/" > /root/.npmrc && \
    echo "@lr:registry=http://npm.leveredreturns.com/" >> /root/.npmrc && \
    echo "//npm.leveredreturns.com/:_authToken=${LR_NPM_TOKEN}" >> /root/.npmrc && \
    echo "//npm.leveredreturns.com/:always-auth=true" >> /root/.npmrc && \
    echo "progress=false" >> /root/.npmrc

ARG NODE_ENV
ARG PORT

# Create the build directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src

# Install node_modules
COPY package.json .
# RUN apk update && \
#  apk add --no-cache python alpine-sdk && \
#  npm install --production && \
#  apk del python alpine-sdk

RUN npm install --production

# Copy build config files
COPY .babelrc .
COPY webpack.prod.js .

# Copy app source files
COPY app/ /usr/src/app

# Setup build environment
ENV NODE_ENV $NODE_ENV
ENV PORT $PORT

# Run asset pipeline build
ENV NODE_PATH lib
RUN node --max_old_space_size=3900 node_modules/webpack/bin/webpack.js -p --env production --config webpack.prod.js --colors --bail

# Run server build
RUN mkdir -p /usr/src/build
RUN cp -rf app/* build/
RUN node_modules/babel-cli/bin/babel.js app --out-dir build/ --ignore app/pages,test/*

ENV NODE_PATH /usr/src/build/lib
WORKDIR /usr/src/build

CMD ["node", "routes/index.js"]
