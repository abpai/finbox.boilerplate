/* eslint-disable */

/**
 * Module Dependencies
 */

const webpack = require('webpack')
const join = require('path').join
const relative = require('path').relative
const resolve = require('path').resolve
const log_update = require('log-update')
const is_uri = require('is-uri')
const theme = require('./app/lib/theme-default')
const merge = require('plain-merge')

/**
 * Context
 */

const app_root = join(__dirname, 'app')
const build_root = join(__dirname, 'build')
const NODE_PATH = process.env.NODE_PATH

/**
 * Configuration
 */

const cssimport = require('postcss-import')
const rucksack = require('rucksack-css')
const vars = require('postcss-simple-vars')
const nested = require('postcss-nested')
const url = require('postcss-url')

/**
 * Plugins
 */

const PrintChunksPlugin = require('./app/lib/webpack-print-chunks-plugin')

/**
 * Loaders
 */

const PostCSSLoader = require('postcss-loader')
const MarkdownLoader = require('markdown-loader')
const BabelLoader = require('babel-loader')
const Styleloader = require('style-loader')
const CSSLoader = require('css-loader')
const URLLoader = require('url-loader')
const FileLoader = require('file-loader')
const HTMLLoader = require('html-loader')

const rules = [
  {
    test: /\.(js|jsx)$/,
    use: [
      {
        loader: 'babel-loader',
        query: {
          babelrc: false,
          presets: [
            [
              'env',
              {
                targets: {
                  browsers: [
                    '> 5%'
                  ],
                  modules: false,
                  loose: true,
                  useBuiltIns: true
                }
              }
            ]
          ],
          plugins: [
            "transform-decorators-legacy",
            'transform-object-rest-spread',
            'transform-strict-mode',
            'transform-es2015-arrow-functions'
          ]
        }
      }
    ],
    exclude: [
      /node_modules/,
      /test\//
    ],
    include: [
      join(app_root, 'pages'),
      join(app_root, 'lib')
    ]
  },
  {
    test: /\.css$/,
    use: [
      'style-loader',
      'css-loader',
      'postcss-loader'
    ]
  },
  {
    test: /\.json$/,
    loader: 'json-loader'
  },
  {
    test: /\.(png|jpg|jpeg|gif|svg)$/,
    loader: 'url-loader?limit=10000'
  },
  {
    test: /\.(woff|woff2)/,
    loader: 'url-loader?limit=10000'
  },
  {
    test: /\.(ttf|eot)$/,
    loader: 'file-loader'
  },
  {
    test: /\.html$/,
    loader: 'html-loader'
  },
  {
    test: /\.(md|markdown)$/,
    use: [
      'html-loader',
      'markdown-loader'
    ]
  }
]

/**
 * Export `config`
 */

module.exports = {
  devtool: 'source-map',
  resolve: {
    modules: [
      resolve(app_root, 'lib'),
      resolve(app_root, 'pages'),
      'node_modules'
    ],
    extensions: [
      '.js',
      '.jsx',
      '.json'
    ]
  },
  entry: {
    signup: join(app_root, 'pages', 'signup', 'signup.js')
  },
  output: {
    path: join(build_root, 'dist'),
    filename: '[name].js',
    publicPath: '/'
  },
  module: { rules },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
      options: {
        context: app_root,
        postcss: postcss()
      }
    }),
    new webpack.DefinePlugin({
      'process.env': Object.keys(process.env).reduce((o, k) => {
        o[k] = JSON.stringify(process.env[k])
        return o
      }, {})
    }),
    new webpack.ProgressPlugin(on_progress),
    new webpack.optimize.UglifyJsPlugin({
      test: /\.jsx?$/,
      minimize: true,
      compress: {
        unused: true,
        dead_code: true,
        screw_ie8: true,
        drop_console: false,
        warnings: false
      },
      output: {
        comments: false
      },
      sourceMap: true
    }),
    new PrintChunksPlugin({
      output: join(__dirname, 'chuck-composition.json')
    })
  ]
}

/**
 * PostCSS
 */

function postcss () {
  const plugins = [
    cssimport({ app_root, path: join(app_root, NODE_PATH) }),
    rucksack({ autoprefixer: true }),
    nested(),
    vars({ variables: () => theme }),
    url()
  ]
  return plugins
}

/**
* On progress
*/

function on_progress (progress, message) {
  log_update('\n  < webpack >  ' + Math.round(progress * 100) + '%  :  ' + (message || 'done') + '\n')
}
